database = [['phil', 'correcthorse', 1000],
            ['sue', 'horsebattery', 1000],
            ['john', 'batterystaple', 500]]


def checkAuth(checkAuthFlag):
    userDatabaseId = None
    while checkAuthFlag == False:
        userpass = input('Enter username followed by password: ')
        for userData in database:
            if userpass.split()[0] == userData[0]:
                if userpass.split()[1] == userData[1]:
                    userDatabaseId = database.index(userData)
                    checkAuthFlag = True
                    balancePrint(userDatabaseId)
                    break
        else:
            print('Try again, wrong input. ')
    return checkAuthFlag, userDatabaseId

def balancePrint(balUserID):
    print('Balance: ' + str(database[balUserID][2]))


def menuPrint():
    print('''
    1 Deposit
    2 Withdraw
    3 Exit
    ''')


def takeInput(numberPress, inputExitFlag, inpUserID):
    if numberPress == 1:
        inpUserID = deposit(inpUserID)
        
    elif numberPress == 2:
        inpUserID = withdraw(inpUserID)
        
    elif numberPress == 3:
        inputExitFlag = True
        
    else:
        print('Wrong input.')
        
    return inputExitFlag, inpUserID


def deposit(depUserID):
    howmuch = int(input('Enter how much to deposit: '))
    database[depUserID][2] += howmuch
    balancePrint(depUserID)
    return depUserID


def withdraw(witUserID):
    howmuch = int(input('Enter how much to withdraw: '))
    if (howmuch > database[witUserID][2]):
        print('Not enough balance')
        balancePrint(witUserID)
        return witUserID
    else:
        database[witUserID][2] -= howmuch
    balancePrint(witUserID)


def main():

    global_exitFlag = False
    global_authFlag = False
    ID = 0
    
    while global_exitFlag == False:
        if global_authFlag == True:
            menuPrint()
            keyPress = int(input())
            global_exitFlag, ID = takeInput(keyPress, global_exitFlag, ID)   
        else:
            global_authFlag, ID = checkAuth(global_authFlag)

if __name__ == "__main__":
    main()