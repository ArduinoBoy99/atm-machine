# ATM-machine

Simulates an ATM machine. Prompts for userid and password. If userid and 
password are not in the database, reprompts, displays balance, presents a menu 
asking to deposit, withdraw or exit.

Database is simulated via lists. Values don't use currency. Exit exits the 
program completley.